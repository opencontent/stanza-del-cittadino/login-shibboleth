FROM rockylinux/rockylinux:8

# from https://shibboleth.net/downloads/service-provider/RPMS/
# add Shibboleth repo
COPY ./shibboleth/etc/yum.repos.d/shibboleth.repo /etc/yum.repos.d/

# install dependencies
RUN yum install -y \
        httpd \
        java-1.8.0-openjdk-headless \
        mod_ssl \
        nss \
        shibboleth \
        unzip \
    && yum -y clean all


# add application paths
COPY ./shibboleth/opt/shibboleth-sp /opt/shibboleth-sp

# add configurations
COPY ./shibboleth/etc/shibboleth/ /etc/shibboleth/
COPY ./apache/conf.d/ /etc/httpd/conf.d/


# copy bootstrap script
COPY ./shibboleth/usr/local/bin/ /usr/local/bin/
RUN chmod +x \
    /usr/local/bin/docker-bootstrap.sh

# run it
EXPOSE 80 443
CMD [ "docker-bootstrap.sh" ]

